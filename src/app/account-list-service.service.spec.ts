import { TestBed, inject } from '@angular/core/testing';

import { AccountListServiceService } from './account-list-service.service';

describe('AccountListServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccountListServiceService]
    });
  });

  it('should be created', inject([AccountListServiceService], (service: AccountListServiceService) => {
    expect(service).toBeTruthy();
  }));
});
